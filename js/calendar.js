
// Array of API discovery doc URLs for APIs used by the quickstart
const DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
const SCOPES = "https://www.googleapis.com/auth/calendar.readonly";

const authorizeButton = $('authorize-button');

/**
 *  On load, called to load the auth2 library and API client library.
 */
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

/**
 *  Initializes the API client library and sets up sign-in state
 *  listeners.
 */
function initClient() {
  gapi.client.init({
    apiKey: API_KEY,
    clientId: CLIENT_ID,
    discoveryDocs: DISCOVERY_DOCS,
    scope: SCOPES
  }).then(function () {
    // Listen for sign-in state changes.
   gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

    // Handle the initial sign-in state.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
  });
}

/**
 *  Called when the signed in status changes, to update the UI
 *  appropriately. After a sign-in, the API is called.
 */
function updateSigninStatus(isSignedIn) {
  if (isSignedIn) {
    //authorizeButton.style.display = 'none';
    listUpcomingEvents();
  } else {
    authorizeButton.style.display = 'block';
  }
}

/**
 *  Sign in the user upon button click.
 */
function handleAuthClick(event) {
  gapi.auth2.getAuthInstance().signIn();
}

/**
 *  Sign out the user upon button click.
 */
function handleSignoutClick(event) {
  gapi.auth2.getAuthInstance().signOut();
}


/**
 * Google Calendar API only provideds the ability to get a certain
 * number of events from a single calendar.  There is no ability to
 * draw from multiple calendars or pick a date range.
 * instead I getting the next 20 events from multiple calendars, 
 * combining the results arrays, sorting them by date, and then taking
 * the first 20 of the combined events array.  
 * If no events are found an appropriate message is printed.
 */

// todo - I could get the list of all calendars I am subscribed to and then itterate through them to get events rather than manually getting and combinining the three.  That would be more generically useful...but I don't think I want all the calendars I am subscribed to on this display.  

// get the first 20 items from multiple calendars
function listUpcomingEvents() {
  var calendar1 = Promise.resolve(
    gapi.client.calendar.events.list({
    'calendarId': 'primary',
    'timeMin': (new Date()).toISOString(),
    'showDeleted': false,
    'singleEvents': true,
    'maxResults': 20,
    'orderBy': 'startTime',
    'access_type': 'offline'
    })
  );

  var calendar2 = Promise.resolve(
    gapi.client.calendar.events.list({
    'calendarId': 'kuragara@gmail.com',
    'timeMin': (new Date()).toISOString(),
    'showDeleted': false,
    'singleEvents': true,
    'maxResults': 20,
    'orderBy': 'startTime',
    'access_type': 'offline'
    })
  );

  var calendar3 = Promise.resolve(
    gapi.client.calendar.events.list({
    'calendarId': 'en.usa#holiday@group.v.calendar.google.com',
    'timeMin': (new Date()).toISOString(),
    'showDeleted': false,
    'singleEvents': true,
    'maxResults': 20,
    'orderBy': 'startTime',
    'access_type': 'offline'

    })
  );

  // When the three calls have returned

  Promise.all([calendar1, calendar2, calendar3]).then(values => { 
    // clear calendar list div
    $('#calendar-list').empty();
    $('#events-today').empty();

    // combine events from the different calendars into one array
    var combinedEvents = [...values[0].result.items, 
                          ...values[1].result.items,
                          ...values[2].result.items];

    // sort events by date
    combinedEvents.sort(function(a, b){
      let dateA, dateB;
      // Google returns either a date or a dateTime for each item. 
      a.start.date ?  dateA = a.start.date : dateA = a.start.dateTime;
      b.start.date ?  dateB = b.start.date : dateB = b.start.dateTime;

      // Turn strings into dates, and then subtract them
      // to get a value that is either negative, positive, or zero.
      return new Date(dateA) - new Date(dateB);
    });

    for(let event of combinedEvents){
      let eventDate, eventDateFormated;
      let today = moment.utc();

      if (event.start.date){
        eventDate = moment.parseZone(event.start.date);
      }else{
        eventDate = moment.parseZone(event.start.dateTime);
        eventDateFormated = eventDate.toLocaleString('en-US');
      }
      // if the event is today also list it under the date and time
      if (moment(today).isSame(eventDate, 'day')){
       $('#events-today').append(`${i.summary}<br>`);
      }
    }

    // reduce to 20 events
     combinedEvents.splice( 21 , combinedEvents.length );

    for (let event of combinedEvents){
      let eventDate;
      let summaryString = event.summary;

      // visually call out reminders
      // todo: when add templating restyle this
      summaryString = summaryString.replace(/reminder/gi, '<em>REMINDER</em>');

      // if the event is a daily event use start.date otherwise use start.datetime
      eventDate = event.start.date ?  moment.parseZone(event.start.date).format('LL') : moment.parseZone(event.start.dateTime).format('LLL');
      $('#calendar-list').append(`<p> ${eventDate}  ${summaryString}</p>`);
    }

      $('#calendar-list').append(`last updated ${moment().format('LLL')}`);
      
  });
}

setInterval(listUpcomingEvents, 300000);
