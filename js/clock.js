function getTime() {
	const monthArray = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
	const dayArray = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
	var timeAlert = "all_good";
	// todo - do I want to use moment.js or is this good enough?
	var timeElement = document.createElement("p");
	var dateElement = document.createElement("p");
	var today = new Date();
	var month = today.getMonth(); // use against Months array
	var date = today.getDate();
	var day = today.getDay();
	var hour = today.getHours();
	var minute = today.getMinutes();
	var time = today.toLocaleString('en-US', { hour: 'numeric',minute:'numeric', hour12: true });
	

	// on weekday mornings change time color and size as it gets closer to leave the house time
  if (day <= 4 && hour === 7){
	  if (minute > 25 && minute < 50){
	 	  timeAlert = "get_ready";
	  } else if (minute >= 50){
	 	  timeAlert = "shoes_and_socks";
		}else{
			timeAlert="all_good";
		}
  }
  if (day <= 4 && hour === 8 && minute <= 10){
  	if( minute <= 10 ){
  		timeAlert = "out_door";
  	}else{
  		timeAlert="all_good"
  	}
  }

  dateElement.innerHTML = dayArray[day] + " " + monthArray[month]+ " " + date;
  timeElement.className = '';
	timeElement.className = timeAlert;
	timeElement.innerHTML = time;
	return  [dateElement, timeElement];

}

function displayTime(){
	let now = getTime();
	$(".date").html(now[0])
	$(".time").html(now[1]);
}

// Display Time and refresh ever 30 seconds.
$(displayTime); 
setInterval(displayTime, 30000);